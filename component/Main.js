import React, {Fragment,Component} from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
  Text,
  Button,
  TextInput
} from 'react-native';

import Note from './Note'

class Main extends Component {

    // constructor(props) {
    //     super(props);
    //     this.state = {
    //         noteArray : [],
    //         noteText: '',
    //     }
    // }

    //in ES7 there is no need for constructor() method.
    state = {
      noteArray : [],
      noteText: "",
  }


  render(){

    let notes = this.state.noteArray.map((val, key) => {
        return <Note key={key} keyval={key} val={val} deleteMethod={ () => this.deleteNote(key)} />
    }); 

    return(
      <View style={styles.container}>
          <View style={styles.header}>
              <Text style={styles.headerText}>Simple Todo List</Text>
          </View>
          <ScrollView style={styles.scrollContainer}>
            {notes}
          </ScrollView>
          <View style={styles.footer}>
              <TextInput
                onChangeText={(noteText) => this.setState({noteText},()=> console.log(this.state.noteArray))}
                value={this.state.noteText}
                style={styles.textInput}
                placeholder='enter doin thing!'
                placeholderTextColor='white'
                underlineColorAndroid='transparent'
                >
              </TextInput>
              <Button onPress={ this.addNote.bind(this) }  style={styles.addButton} title='add doing stuff!'></Button>
          </View>
      </View>
    )
  }

  addNote() {
       if (this.state.noteText) {
           this.state.noteArray.push({
               'note' : this.state.noteText
           });
           this.setState({noteArray : this.state.noteArray})
           this.setState({noteText:''}); 
       }
  }

  deleteNote(key){
      this.state.noteArray.splice(key, 1);
      this.setState({ noteArray: this.state.noteArray })
  }
} 

const styles = StyleSheet.create({
    container: {
      flex: 1
    },
    header: {
      backgroundColor: "#E91E63",
      alignItems: "center",
      justifyContent: "center",
      borderBottomWidth: 10,
      borderBottomColor: "#ddd"
    },
    headerText: {
      color: "#ddd",
      fontSize: 18,
      padding: 15
    },
    scrollContainer: {
      flex: 1,
      marginBottom: 100 // for not offscreen items of Notes when quantities increased. 
    },
    footer: {
      position: "absolute",
      bottom: 0,
      left: 0,
      right: 0,
    },
    textInput: {
      alignSelf: "stretch",
      color: "#fff",
      padding: 20,
      backgroundColor: "#252525",
      borderTopWidth: 2,
      borderTopColor: "#ededed"
    },
    addButton: {
      right: 20,
      bottom: 90,
      width: 90,
      height: 90,
      borderRadius: 59,
    }
});

export default Main;