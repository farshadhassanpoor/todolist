import React, {Fragment,Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Button
} from 'react-native';


class Note extends Component {
  render(){
    return(
      <View key={this.props.keyval} style={styles.note}>
        <Text style={styles.noteText}>{this.props.val.note}</Text>
         <Button
          style={styles.noteDelete}
          onPress={this.props.deleteMethod}
          title="D"
          color="#f194ff"
          />
      </View>
    )
  }
}




const styles = StyleSheet.create({
    note: {
        flex : 1,
        padding: 20,
        borderBottomWidth: 2,
        borderBottomColor: '#ededed',
        alignContent: 'space-between',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    noteText: {
        paddingLeft: 20,
        borderLeftWidth: 1,
        borderLeftColor: '#e91e63', 
        
    }, // Red Box Besides Notes Items.
    noteDelete: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2980b9',
        marginLeft: 110
    },
    noteDeleteText: {
        color: 'white',
        marginTop: 30
    }
});

export default Note;